import { Router} from "@angular/router";
import { Injectable } from '@angular/core';
import { Http , Response,Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { ISignin } from '../../user-interfaces/signin-user-interface/isignin';


@Injectable()
export class SigninService{
	//let leng =0;
	p:number=1;
	//Trash	
	

	private _forgotPassword= 'http://localhost:8009/recoverpassword';
	private _usersLoginUrl= 'http://localhost:8009/signin';
	
	
	constructor(private _http: Http, private _router:Router){}
	
	//////////////////////////////////////////////////////////////////////GET///////////////////////////////////////////////////////////
	
	authenticateUsers(userRole:string,userName:string,pwd:string):Observable<ISignin[]>
	{
		let username:string ="user";		
		let password:string ="e207f1bd-b87e-41b4-b11f-a78f3cea1671";		
		let headers = new Headers({"Accept": "application/json","Content-Type":"application/json"});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userRole":userRole, "userName":userName, "password": pwd, "isDeleted":0});
		 return  this._http.post(this._usersLoginUrl,body,{headers:headers})
		.map((response: Response) => <ISignin[]> response.json())
		.do(data => { localStorage.setItem("userData",JSON.stringify(data));
		this._router.navigate(["/clients"]);
			}
		);		
	}
	
	recoverPassword(authUserIdFk:string,userRole:string,isDeleted:string):Observable<ISignin[]>
	{
		let username:string ="user";		
		let password:string ="e207f1bd-b87e-41b4-b11f-a78f3cea1671";		
		let headers = new Headers({"Accept": "application/json","Content-Type":"application/json"});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userRole":userRole, "password": password, "isDeleted":0});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._usersLoginUrl,body,{headers:headers})
		.map((response: Response) => <ISignin[]> response.json());		
	}
	
	
	
	
	
	
	//error/exceptions
	private handleError(error: Response)
	{
	 console.error(error);
	 return Observable.throw(error.json().error());
	}
} 