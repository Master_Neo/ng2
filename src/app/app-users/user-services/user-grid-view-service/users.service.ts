import { Router} from "@angular/router";
import { Injectable } from '@angular/core';
import { Http , Response,Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { IUser } from '../../user-interfaces/grid-view-interface/iuser';


@Injectable()
export class UsersService{
	//let leng =0;
	p:number=1;
	//Trash	
	private _usersIsDeleteUrl= 'http://localhost:8009/isuserdeleted';
	
	//create/save	
	private _createUsersUrl= 'http://localhost:8009/createusers';	
	private _updateUsersUrl = 'http://localhost:8009/updateusers';
/////////////////////////////////////////////////////////////////////////////////USERS

	private _getAllUsersUrl= 'http://localhost:8009/getauthusers';
	private _usersLoginUrl= 'http://localhost:8009/login';
	
	
	constructor(private _http: Http, private _router:Router){}
	
	//////////////////////////////////////////////////////////////////////GET///////////////////////////////////////////////////////////
	
	//Get all users 
	getUsers(): Observable<IUser[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		return this._http.get(this._getAllUsersUrl,{headers:headers})
		.map((response: Response) => <IUser[]> response.json());
	}
	/////////////////////////////////////////////////////////////////////EDIT ///////////////////////////////////////////////////
	editUser(userId:number,lName:string,fName:string,idNumber:string,isDeleted:string):Observable<IUser[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userId":userId,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._updateUsersUrl,body,{headers:headers})
		.map((response: Response) => <IUser[]> response.json())
		
		;		
	}
	/////////////////////////////////////////////////////////////////////SAVE ///////////////////////////////////////////////////
	saveUser(lName:string,fName:string,idNumber:string,isDeleted:string):Observable<IUser[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._createUsersUrl,body,{headers:headers})
		.map((response: Response) => <IUser[]> response.json());		
	}
	//////////////////////////////////////////////LOGIN//////////////////////////////////////////////////////////////////////////
	deleteUser(userRole:string,userId:number,lName:string,fName:string,idNumber:string,isDeleted:string):Observable<IUser[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userRole":userRole,"userId":userId,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._usersIsDeleteUrl,body,{headers:headers})
		.map((response: Response) => <IUser[]> response.json())
		.do(data =>{ this._router.navigate(["/usertrash"]);
		});
		
				
	}
	//error/exceptions
	private handleError(error: Response)
	{
	 console.error(error);
	 return Observable.throw(error.json().error());
	}
} 