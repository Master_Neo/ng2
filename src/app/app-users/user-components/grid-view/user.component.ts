import { Router} from "@angular/router";
import { Component, Input } from '@angular/core';
import { IUser } from '../../user-interfaces/grid-view-interface/iuser';
import { UsersService } from '../../user-services/user-grid-view-service/users.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({ 

	selector: 'my-app',
	templateUrl:  './users.component.html',
	providers: [UsersService]
})

export class UserComponent {
	columns =[
	{prop:'name'},
	{name:"fname"},
	{name:'lname'}
	]
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	//USERS
	iusers: IUser[]=[];
	iusersById: IUser[]=[];
	iusersSave: IUser[]=[];
	
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	userId = 0;
	lName = "";
	fName = "";
	idNumber = "";
	isDeleted	= "";

	userRole = "God";

	
	
	constructor(private _user: UsersService, private _router: Router){
			
	}
	
	////////////////////////////////////////////////////////////////////USERS DETAILS///////////////////////////////////////////////////////
	//GET users
	ngOnInit() : void{
	
	this._user.getUsers()
	.subscribe(iusers => this.iusers=iusers);
	}
	
	////////////////////////////////////////////////////////////////////SYNC DATA///////////////////////////////////////////////////////
	
	
	
		postData():void{
			
			this._user.editUser(this.userId,this.lName,this.fName,this.idNumber,this.isDeleted)
			.subscribe(iusers => this.iusers=iusers);
		
		}
		
		postDeleteData(user:any):void{
			this.userId = user['userId'];
			this.lName = user['lname'];
			this.fName = user['fname'];
			this.idNumber = user['idNumber'];
			this.isDeleted	= "1";
			
			this._user.deleteUser(this.userRole,this.userId,this.lName,this.fName,this.idNumber,this.isDeleted)
			.subscribe(iusers => this.iusers=iusers);
		
		}
		signOut():void{
			let empty="";
				
		 window.localStorage.removeItem("userData");
		 this._router.navigate(['/signin']);
		}
	
} 