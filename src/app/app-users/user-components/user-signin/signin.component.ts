import { Router} from "@angular/router";
import { Component, Input } from '@angular/core';
import { ISignin } from '../../user-interfaces/signin-user-interface/isignin';
import { SigninService } from '../../user-services/signin-user-service/signin.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({ 

	selector: 'my-app',
	templateUrl:  '../../user-signin/signin.user.component.html',
	providers: [SigninService]
})

export class SigninComponent {
	
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	
	isigninusers: ISignin[]=[];
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	pwd = "";
	userName = "";
	userRole = "";//role
	isDeleted	= "";//is user active
	
	//forgot passsword 
	email="";
	
	constructor(private _signinUser: SigninService, private _router:Router){
		let testAuth = localStorage.getItem("userData");
			if(testAuth){
				console.log("success");
								
			}		
	}
	ngOnInit() : void{
		
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	forgotPassword():void{
			
		}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	signIn():void{
			this._signinUser.authenticateUsers(this.userRole,this.userName,this.pwd)
			.subscribe(isigninusers => this.isigninusers=isigninusers);		
		}
	
} 