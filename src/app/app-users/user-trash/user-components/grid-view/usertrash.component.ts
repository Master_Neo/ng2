import { Router} from "@angular/router";
import { Component, Input } from '@angular/core';
import { IUser } from '../../user-interfaces/grid-view-interface/iuser';
import { UsersService } from '../../user-services/user-grid-view-service/users.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({ 

	selector: 'my-app',
	templateUrl:  './trash.component.html',
	providers: [UsersService]
})

export class UsertrashComponent {
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	//USERS
	iusers: IUser[]=[];
	iusersById: IUser[]=[];
	iusersSave: IUser[]=[];
	
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	userId 		= 0;
	lName 		= "";
	fName 		= "";
	idNumber 	= "";
	isDeleted	= 0;
	userRole	="God";
	password    = "";
	userName    = "";
	authUserId  = 0;
	constructor(private _user: UsersService){
			
	}
	
	////////////////////////////////////////////////////////////////////USERS DETAILS///////////////////////////////////////////////////////
	//GET users
	ngOnInit() : void{
	
	this._user.getUsers()
	.subscribe(iusers => this.iusers=iusers);
	}
	
	
	postData():void{
			this._user.editUser(this.userId,this.lName,this.fName,this.idNumber,this.isDeleted, this.authUserId,this.password, this.userName, this.userRole)
			.subscribe(iusers => this.iusers=iusers);		
		}
		
	postRestoreData(user:any):void{
		
		this.userId = user['userId'];
		this.lName = user['lname'];
		this.fName = user['fname'];
		this.idNumber = user['idNumber'];
		this.isDeleted	= 0;
			
		this._user.restoreUser(this.userId,this.lName,this.fName,this.idNumber,this.isDeleted, this.authUserId,this.password, this.userName, this.userRole)
		.subscribe(iusers => this.iusers=iusers);
		
	}
} 