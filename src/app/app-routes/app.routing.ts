import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {TrashComponent} from "../app-client-trash/client-trash-component/trash.component";
import {UsertrashComponent} from "../app-users/user-trash/user-components/grid-view/usertrash.component";
import {ClientComponent} from "../app-clients/client-component/client.component";
import {SigninComponent} from "../app-users/user-components/user-signin/signin.component";
import {UserComponent} from "../app-users/user-components/grid-view/user.component";
import {SaveComponent} from '../app-clients/client-component/save.component';

const APP_ROUTES: Routes = [
	{path:'signin',redirectTo:"/signin", pathMatch:"full"},
	{path:'clients',component:ClientComponent},
	{path:'clientstrash',component:TrashComponent},
	{path:'usertrash',component:UsertrashComponent},	
	{path:'users',component:UserComponent},
	{path:'saveclient',component:SaveComponent},	
	{path:'signin',component:SigninComponent},
					  
];
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
