import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormlyModule } from '@ngx-formly/core';
import {ReactiveFormsModule} from '@angular/forms';
import { FormlyMaterialModule } from '@ngx-formly/material';


import { UsertrashComponent} from "../app-users/user-trash/user-components/grid-view/usertrash.component";
import { SigninComponent } from '../app-users/user-components/user-signin/signin.component';
import { UserComponent } from '../app-users/user-components/grid-view/user.component';
import { SaveComponent } from '../app-clients/client-component/save.component';
import { TrashComponent } from '../app-client-trash/client-trash-component/trash.component';
import { ClientComponent } from '../app-clients/client-component/client.component';
import { HeaderComponent } from '../app/header.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpModule } from '@angular/http';
import { routing } from "../app-routes/app.routing";
import { GrowlModule } from "ngx-growl";

//import {jQueryToken } from ' ../app-clients/client-component/jquery.service';
//import { DataTablesModule } from 'angular-datatables';

@NgModule({
 imports: [FormlyMaterialModule,ReactiveFormsModule, FormlyModule,BrowserAnimationsModule, GrowlModule.forRoot({maxMessages:10,displayTimeMs:5000}),BrowserModule, HttpModule, FormsModule, routing, NgxPaginationModule],
 declarations: [ ClientComponent,HeaderComponent,SaveComponent,UserComponent,SigninComponent,TrashComponent,UsertrashComponent],
 bootstrap: [ HeaderComponent ]
})
export class AppModule { }

