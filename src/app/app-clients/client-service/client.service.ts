import { Router} from "@angular/router";
import { Injectable } from '@angular/core';
import { Http , Response,Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { IClient } from '../client-interface/iclient';


@Injectable()
export class ClientService{
	
	//Trash	
	private _clientIsDeleteUrl= 'http://localhost:8009/isclientdeleted';
	//get all clients
	private _clienturl= 'http://localhost:8009/getallactiveclients';
	
	//create/save	and edit
	private _clienCreateturl= 'http://localhost:8009/createclient';	
	private _updateClientUrl = 'http://localhost:8009/updateclient';


	
	constructor(private _http: Http, private _router:Router){}
	
	//////////////////////////////////////////////////////////////////////GET///////////////////////////////////////////////////////////
	getClients(): Observable<IClient[]>
	{
		let username:string ="user";
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		return this._http.get(this._clienturl,{headers:headers})
		.map((response: Response) => <IClient[]> response.json());
	}
	/////////////////////////////////////////////////////////////////////EDIT ///////////////////////////////////////////////////
	editClient(userId:string,userLname:string,userFname:string,userIdNumber:string,userIsDeleted:string,clientId:string,lName:string,fName:string,idNumber:string,isDeleted:string,clientContactDetailsId:string,mobileNumber:string,email:string,clientAddressDetailsId:string,streetNumber:string, streetName: string, surbub:string, postalCode:string,userRole:string):Observable<IClient[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userId":userId,"userLname":userLname,"userFname":userFname,"userIdNumber":idNumber,"userIsDeleted":userIsDeleted,"clientContactDetailsId":clientContactDetailsId,"email":email,"mobileNumber":mobileNumber,"clientAddressDetailsId":clientAddressDetailsId,"streetNumber":streetNumber, "streetName": streetName, "surbub":surbub, "postalCode":postalCode,"clientId":clientId,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted,"userRole":userRole});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._updateClientUrl,body,{headers:headers})
		.map((response: Response) => <IClient[]> response.json())
		.do(data =>{ this._router.navigate(["/clients"])});
		
				
	}/////////////////////////////////////////////////////////////////////DELETE ///////////////////////////////////////////////////
	
	saveClient(userId:string,userLname:string,userFname:string,userIdNumber:string,userIsDeleted:string,lName:string,fName:string,idNumber:string,isDeleted:string,mobileNumber:string,email:string,streetNumber:string, streetName: string, surbub:string, postalCode:string,userRole:string):Observable<IClient[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userId":userId,"userLname":userLname,"userFname":userFname,"userIdNumber":idNumber,"userIsDeleted":userIsDeleted,"email":email,"mobileNumber":mobileNumber,"streetNumber":streetNumber, "streetName": streetName, "surbub":surbub, "postalCode":postalCode,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted,"userRole":userRole});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._clienCreateturl,body,{headers:headers})
		.map((response: Response) => <IClient[]> response.json())
		.do(data =>{ this._router.navigate(["/clients"])});
		
				
	}
	
	deleteClient(userId:string,userLname:string,userFname:string,userIdNumber:string,userIsDeleted:string,clientId:string,lName:string,fName:string,idNumber:string,isDeleted:string,clientContactDetailsId:string,mobileNumber:string,email:string,clientAddressDetailsId:string,streetNumber:string, streetName: string, surbub:string, postalCode:string,userRole:string):Observable<IClient[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"clientAddressDetailsId":clientAddressDetailsId,"streetNumber":streetNumber, "streetName": streetName, "surbub":surbub, "postalCode":postalCode,"clientId":clientId,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted,"clientContactDetailsId":clientContactDetailsId,"email":email,"mobileNumber":mobileNumber,"userRole":userRole,"userId":userId,"userLname":userLname,"userFname":userFname,"userIdNumber":idNumber,"userIsDeleted":userIsDeleted});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._clientIsDeleteUrl,body,{headers:headers})
		.map((response: Response) => <IClient[]> response.json())
		.do(data => {this._router.navigate(["/trash"]);});
		
				
	}
	//error/exceptions/////////////////////////////////////////////////////////////////////
	private handleError(error: Response)
	{
	 console.error(error);
	 return Observable.throw(error.json().error());
	}
} 