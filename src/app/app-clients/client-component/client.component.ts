import { Router} from "@angular/router";
import { Component, Input, Inject } from '@angular/core';
//import { jQueryToken } from './jquery.service';
import { IClient } from '../client-interface/iclient';
import { ClientService } from '../client-service/client.service';
//import { appService } from './app/user/service/user.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { GrowlService } from "ngx-growl";


declare var jQuery: any;

@Component ({ 

	selector: 'my-app',
	templateUrl:  './client.component.html',
	providers: [ClientService]
})

export class ClientComponent {
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	//CLIENTS
	iclients: IClient[]=[];
	iclientById: IClient[]=[];
	iclientSave: IClient[]=[];
	
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	clientId = "";
	lName = "";
	fName = "";
	idNumber = "";
	isDeleted	= "";
	userRole = "God";
	//id's
	//data to sync into models
	clientAddressDetailsId:string;
	clientContactDetailsId :string;
	//address details
	streetName = "";
	streetNumber = "";
	surbub = "";
	postalCode =""  ;
	//contact details
	mobileNumber="";
	email = "";
	//////////user
	userId = "";
	userLname = "";
	userFname = "";
	userIdNumber = "";
	userIsDeleted	= "";
	userData:any[] = [];
	//declare var $ : any;
	
	constructor(private growlService:GrowlService ,private _client: ClientService, private _router:Router){
		let userData = localStorage.getItem("userData");
			if(!userData){
				console.log("Error");
				window.location.href="/signin";				
			}
			this.growlService.addError("Error !!!!!!!!!!!!!");
			 
	
	//this.userData = localStorage.getItem("userData");
	this._client.getClients()
	.subscribe(iclients => this.iclients=iclients);			
	}
	
	////////////////////////////////////////////////////////////////////CLIENT DETAILS///////////////////////////////////////////////////////
	//GET CLIENT
	
	
	////////////////////////////////////////////////////////////////////SYNC DATA///////////////////////////////////////////////////////
	
	syncClientData(client:any) : void{
		
	
	
	this.clientId = client['clientId'];
	console.log(this.clientId);
	this.lName = client['lName'];
	this.fName = client['fName'];
	this.idNumber = "456789897844425";
	this.isDeleted	= "0";
	
	this.clientContactDetailsId = client.clientContact['clientContactDetailsId'];
	this.email = client.clientContact['email'];
	this.mobileNumber =client.clientContact['mobileNumber'];	
	
	this.clientAddressDetailsId = client.clientAddress['clientAddressDetailsId'];
	this.streetName = client.clientAddress['streetName'];
	this.streetNumber = client.clientAddress['streetNumber'];
	this.postalCode = client.clientAddress['postalCode'];
	this.surbub = client.clientAddress['surbub'];
	
		
			
		
	
	}
	///////////////////////////////////////////////////////POST ////////////////////////////////////////////////
		postEditData():void{
		
			this.userId = "1";
			this.userLname = "Geo";
			this.userFname = "Neo";
			this.userIdNumber = "12345678123456";
			this.userIsDeleted	= "0";
				
					
			this._client.editClient(this.userId,this.userLname,this.userFname,this.userIdNumber,this.userIsDeleted,this.clientId,this.lName,this.fName,this.idNumber,this.isDeleted,this.clientContactDetailsId,this.mobileNumber,this.email,this.clientAddressDetailsId , this.streetNumber, this.streetName, this.surbub, this.postalCode,this.userRole)
			.subscribe(iclients => this.iclients=iclients);
			
			this._client.getClients()
			.subscribe(iclients => this.iclients=iclients);
			//location.reload();
		
		}
		postDeleteData(client:any):void{
		
			
			this.clientId = client['clientId'];
			this.lName = client['lName'];
			this.fName = client['fName'];
			this.idNumber = "4567898978";
			this.isDeleted	= "1";
			
			this.userId = client.user['userId'];
			this.userLname = client.user['lname'];
			this.userFname = client.user['fname'];
			this.userIdNumber = client.user['idNumber'];
			this.userIsDeleted	= "1";
			
			this.clientContactDetailsId = client.clientContact['clientContactDetailsId'];
			this.email = client.clientContact['email'];
			this.mobileNumber =client.clientContact['mobileNumber'];	
			
			this.clientAddressDetailsId = client.clientAddress['clientAddressDetailsId'];
			this.streetName = client.clientAddress['streetName'];
			this.streetNumber = client.clientAddress['streetNumber'];
			this.postalCode = client.clientAddress['postalCode'];
			this.surbub = client.clientAddress['surbub'];
			
			this.userRole = "God";
			
			this._client.deleteClient(this.userId,this.userLname,this.userFname,this.userIdNumber,this.userIsDeleted,this.clientId,this.lName,this.fName,this.idNumber,this.isDeleted,this.clientContactDetailsId,this.mobileNumber,this.email,this.clientAddressDetailsId , this.streetNumber, this.streetName, this.surbub, this.postalCode,this.userRole)
			.subscribe(iclients => this.iclients=iclients);
			//location.reload();
			this._client.getClients()
			.subscribe(iclients => this.iclients=iclients);
		
		}
		
		syncUserData():void{
			//this.userData = localStorage.getItem("userData");
			this._router.navigate(['/saveclient']);
		}
		
		postSaveData(user:any):void{
		
			this.userId = user.user["userId"];
			
			this.userLname = user.user["userLname"];
			this.userFname = user.user["userFname"];
			this.userIdNumber = user.user["userIdNumber"];
			this.userIsDeleted = user.user["userIsDeleted"];
			console.log(this.userFname);		
			this.isDeleted	= "1";
			
			this.userIsDeleted	= "1";			
			
			this.userRole = "God";
			
			this._client.deleteClient(this.userId,this.userLname,this.userFname,this.userIdNumber,this.userIsDeleted,this.clientId,this.lName,this.fName,this.idNumber,this.isDeleted,this.clientContactDetailsId,this.mobileNumber,this.email,this.clientAddressDetailsId , this.streetNumber, this.streetName, this.surbub, this.postalCode,this.userRole)
			.subscribe(iclients => this.iclients=iclients);
			
		}
		signOut():void{
			let empty="";
				
		 window.localStorage.removeItem("userData");
		 this._router.navigate(['/signin']);
		}
} 