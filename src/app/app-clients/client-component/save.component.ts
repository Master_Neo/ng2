import { Router} from "@angular/router";
import { Component, Input } from '@angular/core';
import { IClient } from '../client-interface/iclient';
import { ClientService } from '../client-service/client.service';
//import { appService } from './app/user/service/user.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({ 

	selector: 'my-app',
	templateUrl:  './save.component.html',
	providers: [ClientService]
})

export class SaveComponent {
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	//CLIENTS
	iclients: IClient[]=[];
	iclientById: IClient[]=[];
	iclientSave: IClient[]=[];
	
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	clientId = "";
	lName = "";
	fName = "";
	idNumber = "";
	isDeleted	= "";
	userRole = "God";
	//address details
	streetName = "";
	streetNumber = "";
	surbub = "";
	postalCode =""  ;
	//contact details
	mobileNumber="";
	email = "";
	//////////user
	userId = "";
	userLname = "";
	userFname = "";
	userIdNumber = "";
	userIsDeleted	= "";
	
	
	constructor(private _client: ClientService, private _router :Router){
		let testAuth = localStorage.getItem("userData");
			if(!testAuth){
				console.log("Error");
				window.location.href="/signin";				
			}
		
	}
	
		postSaveData():void{
			let userData = localStorage.getItem("userData");
			 
			let savedData = JSON.parse(userData);
		 
			 for(let key in savedData ){
				 
				this.userId = savedData['user']['userId'];
				this.userLname = savedData['user']['lname'];
				this.userFname = savedData['user']['fname'];
				this.userIdNumber = savedData['user']['idNumber'];
				this.userIsDeleted	=  savedData['user']['isDeleted'];
			 }		 
			
			
			this._client.saveClient(this.userId,this.userLname,this.userFname,this.userIdNumber,this.userIsDeleted,this.lName,this.fName,this.idNumber,this.isDeleted,this.mobileNumber,this.email , this.streetNumber, this.streetName, this.surbub, this.postalCode,this.userRole)
			.subscribe(iclients => this.iclients=iclients);
			
		}
	signOut():void{
			let empty="";
				
		 window.localStorage.removeItem("userData");
		 this._router.navigate(['/signin']);
		}
} 