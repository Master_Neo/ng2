import { Router} from "@angular/router";
import { Injectable } from '@angular/core';
import { Http , Response,Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { ITrash } from '../client-trash-interface/itrash';


@Injectable()
export class TrashService{
	
	//Trash	
	private _clientIsRestoredUrl= 'http://localhost:8009/isclientdeleted';
	
	//get all clients
	private _clieninactiveurl= 'http://localhost:8009/getallinactiveclients';
	
	constructor(private _http: Http, private _router:Router){}
	
	//////////////////////////////////////////////////////////////////////GET///////////////////////////////////////////////////////////
	getClients(): Observable<ITrash[]>
	{
		let username:string ="user";
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		return this._http.get(this._clieninactiveurl,{headers:headers})
		.map((response: Response) => <ITrash[]> response.json());
	}
	
	/////////////////////////////////////////////////////////////////////DELETE ///////////////////////////////////////////////////
	restoreClient(userRole:string,userId:string,userLname:string,userFname:string,userIdNumber:string,userIsDeleted:string,clientId:string,lName:string,fName:string,idNumber:string,isDeleted:string,clientContactDetailsId:string,mobileNumber:string,email:string,clientAddressDetailsId:string,streetNumber:string, streetName: string, surbub:string, postalCode:string):Observable<ITrash[]>
	{
		let username:string ="user";		
		let password:string ="6c85e10f-b802-4840-96a2-2cbe6b4c963a";		
		let headers = new Headers({'Accept': 'application/json'});		
		headers.append("Authorization", "Basic " + btoa(username + ":" + password));
		
		const body = JSON.stringify({"userRole":userRole,"userId":userId,"userLname":userLname,"userFname":userFname,"userIdNumber":idNumber,"userIsDeleted":userIsDeleted,"clientAddressDetailsId":clientAddressDetailsId,"streetNumber":streetNumber, "streetName": streetName, "surbub":surbub, "postalCode":postalCode,"clientId":clientId,"lName":lName,"fName":fName,"idNumber":idNumber,"isDeleted":isDeleted,"clientContactDetailsId":clientContactDetailsId,"email":email,"mobileNumber":mobileNumber});
		 //const headers = new Headers({"Content-Type":"application/json"});
		 return  this._http.post(this._clientIsRestoredUrl,body,{headers:headers})
		.map((response: Response) => <ITrash[]> response.json())
		.do(data => this._router.navigate(["/clients"]));				
	}
	
	//error/exceptions/////////////////////////////////////////////////////////////////////
	private handleError(error: Response)
	{
	 console.error(error);
	 return Observable.throw(error.json().error());
	}
} 