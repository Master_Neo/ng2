export interface ITrash{
	
 userRole: string;
 clientId: number;
 fName: string;
 lName: string;
 resId:number;
 contactId:number;
 idnumber:number;
 
 clientContactDetailsId: number;
 email: string;
 mobileNumber: string;
 
 clientAddressDetailsId:number;
 streetNumber:string;
 streetName:string;
 postalCode:string;
 surbub:string;
 
}
