import { Router} from "@angular/router";
import { Component, Input } from '@angular/core';
import { ITrash } from '../client-trash-interface/itrash';
import { TrashService } from '../client-trash-service/trash.service';
//import { appService } from './app/user/service/user.service';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({ 

	selector: 'my-app',
	templateUrl:  './trash.component.html',
	providers: [TrashService]
})

export class TrashComponent {
	p:number=1;
	//Interface/blue print for server API///////////////////////////////////////DATA ARRAYS//////////////////////////////////
	//CLIENTS
	itrash: ITrash[]=[];
	
	////////////////////////////////////////////////////////////////////////////MEMBERS///////////////////////////////////////
	clientId = "";
	lName = "";
	fName = "";
	idNumber = "";
	isDeleted	= "";
	userRole ="";
	//id's
	//data to sync into models
	clientAddressDetailsId:string;
	clientContactDetailsId :string;
	//address details
	streetName = "";
	streetNumber = "";
	surbub = "";
	postalCode =""  ;
	//contact details
	mobileNumber="";
	email = "";
	//////////user
	userId = "";
	userLname = "";
	userFname = "";
	userIdNumber = "";
	userIsDeleted	= "";
	
	constructor(private _client: TrashService,private _router:Router){
		let testAuth = localStorage.getItem("userData");
			if(!testAuth){
				console.log("Error");
				this._router.navigate(["/clients"]);				
			}

	this._client.getClients()
	.subscribe(itrash => this.itrash=itrash);			
	}
	postRestoreData(client:any):void{
		
		   	
			this.userId = client.user['userId'];
			this.userLname = client.user['lname'];
			this.userFname = client.user['fname'];
			this.userIdNumber = client.user['idNumber'];;
			this.userIsDeleted	= "1";
			
			
			
			this.clientId = client['clientId'];
			this.lName = client['lName'];
			this.fName = client['fName'];
			this.idNumber = "4567898978";
			this.isDeleted	= "0";
			
			this.clientContactDetailsId = client.clientContact['clientContactDetailsId'];
			this.email = client.clientContact['email'];
			this.mobileNumber =client.clientContact['mobileNumber'];	
			
			this.clientAddressDetailsId = client.clientAddress['clientAddressDetailsId'];
			this.streetName = client.clientAddress['streetName'];
			this.streetNumber = client.clientAddress['streetNumber'];
			this.postalCode = client.clientAddress['postalCode'];
			this.surbub = client.clientAddress['surbub'];
			
			this.userRole = "God";
			
			this._client.restoreClient(this.userRole,this.userId,this.userLname,this.userFname,this.userIdNumber,this.userIsDeleted,this.clientId,this.lName,this.fName,this.idNumber,this.isDeleted,this.clientContactDetailsId,this.mobileNumber,this.email,this.clientAddressDetailsId , this.streetNumber, this.streetName, this.surbub, this.postalCode)
			.subscribe(itrash => this.itrash=itrash);
			//location.reload();
			
		
		}
		
		signOut():void{
			let empty="";
				
		 window.localStorage.removeItem("userData");
		 this._router.navigate(['/signin']);
		}
	
} 